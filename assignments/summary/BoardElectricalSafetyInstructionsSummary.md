## Broad Electrical Safety Instructions

### 1. Power Supply
**Do’s:**

 - Always make sure that the output voltage of the power supply matches the input voltage of the board.
 - While turning on the power supply make sure every circuit is connected properly.

**Don’ts:**

 - Do not connect power supply without matching the power rating.
 - Never connect a higher output(12V/3A) to a lower (5V/2A) input .
 - Do not try to force the connector into the power socket ,it may damage the connector.

### 2. Handling
**Do’s:**

 - Treat every device like it is energized, even if it isn't plugged in or operational.
 - While working keep the board on a flat stable surface (say, wooden table).
 - Unplug the device before performing any operation on them.
 - When handling electrical equipment, make sure your hands are dry.
 - Keep all electrical circuit contact points enclosed.
 - If the board becomes too hot try to cool it with an external fan .

**Don’ts:**

 - Don’t handle the board when it's powered ON.
 - Never touch electrical equipment when any part of your body is wet, (this includes fair amounts of perspiration).
 - Do not touch any sort of metal to the development board.

### 3. Using interfaces(GPIO, UART,I2C,SPI)

### - GPIO

**Do’s:**

 - Find out whether the board runs on 3.3v or 5v logic.
 - Always connect the LED (or sensors) using appropriate resistors .
 - To use 5V peripherals with 3.3V, we require a logic level converter.

**Don’ts:**

 - Never connect anything greater that 5V to a 3.3V pin.
 - Avoid making connections when the board is running.
 - Don't plug anything with a high (or negative) voltage.
 - Do not connect a motor directly , use a transistor to drive it.

### - UART

 - Connect Rx pin of *device1* to Tx pin of *device2* ,similarly Tx pin of *device1* to Rx pin of *device2*.
 - If *device1* works on 5V and *device2* works at 3.3V then use the level shifting mechanism(voltage divider)
 - Generally UART is used to communicate with board through USB to TTL connection. USB to TTL connection does not require a protection circuit .
 - Whereas sensor interfacing using UART might require a protection circuit.

### - I2C

 - While using I2C interfaces with sensors, SDA and SDL lines must be protected.
 - Protection of these lines is done by using pullup registers on both lines.
 - If you use the inbuilt pullup registers in the board you wont need an external circuit.
 - If you are using bread-board to connect your sensor, use the pullup resistor. Generally , 2.2kohm <= 4K ohm resistors are used.

### - SPI

 - Generally ,SPI in development boards is in push-pull mode.
 - Push-pull mode does not require any protection circuit.
 - If you are using more than one slave it is possible that  **device2** can "hear" and "respond" to the master's communication with device1 - which is a disturbance. 
 - To overcome this problem , we use a protection circuit with pullup resistors on each of the slave select lines(CS).
 - Resistor's value can be between 1kOhm - 10kOhm. Generally 4.7kOhm resistor is used.

*Refer to this link for protection circuits: [Protection Circuits](https://elinux.org/RPi_Tutorial_EGHS:GPIO_Protection_Circuits)*
